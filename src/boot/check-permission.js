export default ({ Vue, store }) => {
  Vue.prototype.$can = (permission) => {
    return store.state.global.user.can.includes(permission)
  }
}
