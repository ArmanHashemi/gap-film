import axios from 'axios'
import Vue from 'vue'
import { Cookies } from 'quasar'
import { store } from 'src/store'

const axiosInstance = axios.create({
  baseURL: 'http://apitest.tek-nic.com/movie',
  header: {
    'Content-Type': 'application/json'
  }
})

const api = async (method, url, data, auth = false) => {
  const token = { Authorization: 'Bearer ' + Cookies.get('token') }
  const options = {
    method: method,
    url: url,
    headers: auth ? token : null,
    data: data || null
  }

  let res = null
  store.dispatch('global/loading', true)
  await axiosInstance(options)
    .then(async (data) => {
      res = {
        status: data.status,
        data: data.data
      }
    }).catch(({ response }) => {
      res = {
        status: response.status,
        data: response.data
      }
    })

  store.dispatch('global/loading', false)
  return res
}
Vue.prototype.$api = api
export { axios, api }
