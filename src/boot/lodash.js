import _ from 'lodash'
import Vue from 'vue'
// leave the export, even if you don't use it
Vue.prototype.$_ = _
