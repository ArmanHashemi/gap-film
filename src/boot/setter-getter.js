export default ({ store, Vue }) => {
  Vue.prototype.$getter = (key) => {
    return store.state.global[key]
  }

  Vue.prototype.$setter = (key, value) => {
    store.dispatch('global/addToState', { key: key, value: value })
  }

  Vue.prototype.$setUrl = (data) => {
    store.dispatch('global/setRouter', data)
  }

  Vue.prototype.$getUrl = () => {
    return {
      key: store.state.global.currentRouteName,
      value: store.state.global.currentRouteParam
    }
  }
}
