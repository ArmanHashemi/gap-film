
export default ({ router, store, Vue }) => {
  router.beforeEach(async (to, from, next) => {
    // is this route need Auth ?????
    if (to.meta.requireAuth) {
      // yes require auth

      // check state if not set request
      if (Object.keys(store.state.global.user).length !== 0) {
        // yes exist in store

        // check for permission
        if (to.meta.permission) {
          if (await store.state.global.user.can.includes(to.meta.can_access)) {
            return next()
          } else {
            console.log('you dont have permission for access this route')
            await router.replace({ name: 'login' })
          }
        } else {
          return next()
        }
      } else {
        // not exist in store send request
        const status = await store.dispatch('global/profile')
        if (status === 'ok') {
          // check for permission
          if (to.meta.permission) {
            if (await store.state.global.user.can.includes(to.meta.can_access)) {
              return next()
            } else {
              console.log('you dont have permission for access this route')
              await router.replace({ name: 'login' })
            }
          } else {
            return next()
          }
        }
      }
    } else {
      // no dont need auth
      return next()
    }
  })
}
