import { api } from 'boot/axios'
import { Cookies } from 'quasar'

export function addToState ({ dispatch, commit }, data) {
  commit('SET_STATE', data)
}
export function setRouter ({ dispatch, commit }, data) {
  commit('SET_ROUTER', data)
}
export async function me ({ dispatch, commit }) {
  if (Cookies.get('token')) {
    const response = await api('POST', 'user/me', null, true)
    if (response.status === 200) {
      commit('SET_USER', response.data)
      commit('AUTH', true)
    }
  }
}
export async function profile ({ dispatch, commit }) {
  if (Cookies.get('token')) {
    const response = await api('POST', 'user/me', null, true)
    if (response.status === 200) {
      commit('SET_USER', response.data)
      commit('AUTH', true)
      return 'ok'
    } else {
      await this.$router.push({ name: 'login' })
    }
  } else {
    await this.$router.push({ name: 'login' })
  }
}
export async function getFirstPage ({ dispatch, commit }) {
  const response = await api('GET', '/FirstPage')
  if (response.status === 200) {
    commit('SET_INDEX_DATA', {
      line1: response.data.result[0].list,
      line2: response.data.result[1].list,
      line3: response.data.result[2].list
    })
  }
  return response
}
export async function getMovieDetails ({ dispatch, commit }, data) {
  console.log(data)
  const response = await api('POST', '/GetContent', JSON.stringify(data))
  if (response.status === 200) {
    commit('SET_STATE', { key: 'movieDetails', value: response.data.result })
  }
  console.log(response)
  return response
}
export function loading ({ dispatch, commit }, toggle) {
  commit('SET_LOADING', toggle)
}
