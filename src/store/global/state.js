export default function () {
  return {
    currentRouteName: '',
    currentRouteParam: '',
    example: '',
    loading: false,
    auth: false,
    user: {},
    line1: [],
    line2: [],
    line3: [],
    movieDetails: ''
  }
}
