export const SET_STATE = (state, data) => {
  state[data.key] = data.value
}
export const SET_ROUTER = (state, data) => {
  state.currentRouteName = data.key
  state.currentRouteParam = data.value
}
export const SET_USER = (state, data) => {
  state.user = data
}
export const SET_LOADING = (state, data) => {
  state.loading = !!data
}
export const AUTH = (state, data) => {
  state.auth = data
}
export const SET_INDEX_DATA = (state, data) => {
  state.line1 = data.line1
  state.line2 = data.line2
  state.line3 = data.line3
}
