const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/',
        component: () => import('layouts/IndexLayout'),
        children: [
          { path: '/', component: () => import('pages/Index.vue'), name: 'main', meta: { requireAuth: false, permission: false, can_access: '' } },
          { path: '/single/:id', component: () => import('pages/Single.vue'), name: 'single', meta: { requireAuth: false, permission: false, can_access: '' } }
        ]
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
