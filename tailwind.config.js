// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      primary: '#f26522',
      secondary: '#2d294e',
      tertiary: '#40aa54',
      quatrenary: '#ef394e',

      blue: colors.blue,
      gray: colors.trueGray,
      grey: colors.gray,
      red: colors.rose,
      green: colors.green,
      yellow: colors.yellow,

      // Zart's
      white: '#fff',
      black: '#000',
      transparent: 'transparent'
    }
  },
  variants: {
    extend: {}
  },
  plugins: [
  ]
}
